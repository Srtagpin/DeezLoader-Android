package com.dt3264.deezloader

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.widget.ContentLoadingProgressBar
import androidx.preference.PreferenceManager
import io.socket.client.Socket
import net.lingala.zip4j.ZipFile
import okhttp3.*
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.system.exitProcess

// Force reloading node data every time the app starts, for debugging only
const val forceReloadNodeData = false

// Constant tag used in logcat
const val tag = BuildConfig.APPLICATION_ID

// Build number of the running app
const val clientBuildNumber = BuildConfig.VERSION_CODE
const val clientVersion = BuildConfig.VERSION_NAME

const val telegramURL = "https://t.me/joinchat/Ed1JxEfoci8sv2dVwTUQ3A"
const val serverURL = "http://localhost:1730"
const val serviceChannelID = "${BuildConfig.APPLICATION_ID}.service"
const val downloadChannelID = "${BuildConfig.APPLICATION_ID}.downloads"

// Notification ID for the NodeJS service
const val serviceNotificationID = 10

// Variables to hold the last app version that ran and whether the landing page has been displayed
var lastBuildNumber = 0
@Volatile var landingDone = false

// Application-wide shared preferences
lateinit var sharedPreferences: SharedPreferences

// Variable that holds whether the service is running or not
@Volatile var isServiceRunning = false

// An object to supply an interface between NodeJS mobile and the wrapper app
object NodeNative {
    // Loads native libraries upon application startup
    init {
        System.loadLibrary("native-lib")
        System.loadLibrary("node")
    }

    // Native method that starts the NodeJS mobile server with the supplied arguments
    @JvmStatic
    external fun startNodeServer(arguments: Array<String>): Int

    // Rx2 components
    lateinit var serviceSocket: Socket
}

class MainActivity : AppCompatActivity() {
    private var serviceCheckTick = Timer()

    // UI elements
    private lateinit var mainExternalButton: AppCompatButton
    private lateinit var mainInternalButton: AppCompatButton
    private lateinit var telegramButton: AppCompatButton
    private lateinit var updateButton: AppCompatButton
    private lateinit var infoView: AppCompatTextView
    private lateinit var updateText: AppCompatTextView
    private lateinit var versionText: AppCompatTextView
    private lateinit var progressBar: ContentLoadingProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        lastBuildNumber = sharedPreferences.getInt("lastBuildNumber", 0)

        mainExternalButton = findViewById(R.id.openAppExternalButton)
        mainInternalButton = findViewById(R.id.openAppInternalButton)
        telegramButton = findViewById(R.id.telegramButton)
        updateButton = findViewById(R.id.updateButton)
        infoView = findViewById(R.id.infoView)
        updateText = findViewById(R.id.updateText)
        versionText = findViewById(R.id.versionText)
        progressBar = findViewById(R.id.progressBar)

        val versionString = "${getText(R.string.actualVersion)} $clientVersion"

        runOnUiThread { // Show the initial loading text and version string
            infoView.text = getText(R.string.initial_launch_load_text)
            versionText.text = versionString
        }

        telegramButton.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(telegramURL)
                }
            )
        }

        mainExternalButton.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(serverURL)
                }
            )
        }

        mainInternalButton.setOnClickListener {
            startActivity(
                Intent(applicationContext, BrowserActivity()::class.java).apply {
                    action = Intent.ACTION_MAIN
                    addCategory(Intent.CATEGORY_LAUNCHER)
                    addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                }
            )
        }

        checkForUpdates()
        checkPermissions()
    }

    // Main methods
    private fun checkPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 100)
    }

    internal fun readyLandingPage() {
        runOnUiThread {
            progressBar.visibility = View.GONE
            telegramButton.visibility = View.VISIBLE
            mainExternalButton.visibility = View.VISIBLE
            mainInternalButton.visibility = View.VISIBLE
            infoView.text = getText(R.string.serverReady)
        }
        landingDone = true
    }

    private fun checkForUpdates() {
        val request = Request.Builder().url("https://pastebin.com/raw/rEubX2Lu").build()
        OkHttpClient().newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                val pasteResult: Array<String> = Objects.requireNonNull<ResponseBody>(response.body).string().split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (clientVersion != pasteResult[0].trim { it <= ' ' }) {
                    val newVersionName = pasteResult[0].trim { it <= ' ' }
                    val changelogText = pasteResult.drop(2).joinToString("\n- ", "\n- ")

                    val updateString = "A new version ($newVersionName) is available.\n$changelogText"
                    val updateLink = pasteResult[1]

                    showUpdater(updateString, updateLink)
                }
            }
        })
    }

    private fun showUpdater(updateString: String, updateLink: String) {
        runOnUiThread {
            updateText.text = updateString

            updateButton.setOnClickListener {
                val updateRequest = DownloadManager.Request(Uri.parse(updateLink)).apply {
                    setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    setMimeType("application/vnd.android.package-archive")
                }

                val downloadManager = applicationContext.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

                downloadManager.enqueue(updateRequest)
            }

            updateText.visibility = View.VISIBLE
            updateButton.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        serviceCheckTick.cancel()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (this@MainActivity.hasWindowFocus()) {
            runOnUiThread {
                AlertDialog.Builder(this@MainActivity).apply {
                    setMessage(getString(R.string.exit_back))
                    setCancelable(true)
                    setPositiveButton(R.string.confirmation) { _, _ ->
                        finish()
                        exitProcess(0)
                    }
                    setNegativeButton(R.string.denial) { dialogInterface, _ ->
                        dialogInterface.cancel()
                    }
                    create().show()
                }
            }
        }
    }

    private fun saveLastUpdateTime() {
        sharedPreferences.edit().apply {
            putInt("lastBuildNumber", clientBuildNumber)
            apply()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay!
                readyNodeApp()
            } else {
                // permission denied, boo!
                Toast.makeText(this, R.string.permission,
                        Toast.LENGTH_LONG).show()
                this.finish()
            }
        }
    }

    // Function to check whether the local assets are up to date, if not update them
    private fun checkIsApkUpdated() {
        if (forceReloadNodeData || lastBuildNumber != clientBuildNumber) {
            runOnUiThread {
                infoView.text = getText(R.string.copying_files_text)
            }

            //Recursively delete any existing nodejs-project.
            val nodeDirOld = applicationContext.filesDir.absolutePath + "/deezerLoader"
            val nodeDir = applicationContext.filesDir.absolutePath + "/deezloader"
            val nodeZipExternal = applicationContext.filesDir.absolutePath + "deezloader.zip"

            val nodeZipInternalStream = applicationContext.assets.open("deezloader.zip")

            val nodeDirReferenceOld = File(nodeDirOld)
            val nodeDirReference = File(nodeDir)
            val nodeZipExternalReference = File(nodeZipExternal)

            if (nodeDirReferenceOld.exists()) {
                nodeDirReferenceOld.deleteRecursively()
            }

            if (nodeDirReference.exists()) {
                nodeDirReference.deleteRecursively()
            }

            if (nodeZipExternalReference.exists()) {
                nodeZipExternalReference.delete()
            }

            // Copy the zip file from the APK assets to the apps private files
            nodeZipInternalStream.copyTo(nodeZipExternalReference.outputStream())
            // Extract it
            ZipFile(nodeZipExternal).extractAll(nodeDir)
            // Update the preference holding the last build number
            saveLastUpdateTime()
            // Delete the zip file after extraction
            nodeZipExternalReference.delete()
        }
    }

    private fun readyNodeApp() {
        checkIsApkUpdated()
        serviceCheckTick.schedule(CheckDeezloaderService(), 0, 10000)
    }

    // Timer that periodically checks whether the service is still running, sometimes it gets killed unexpectedly
    private inner class CheckDeezloaderService : TimerTask() {
        override fun run() {
            if (!isServiceRunning) {
                Thread {
                    runOnUiThread {
                        infoView.text = getText(R.string.starting_server_text)
                    }
                    startService(
                        Intent(applicationContext, DeezloaderService()::class.java).apply {
                            action = "Deezloader Service"
                        }
                    )
                }.start()
                while (!landingDone) {
                    if (isServiceRunning) {
                        readyLandingPage()
                        break
                    }
                }
            }
        }
    }
}
